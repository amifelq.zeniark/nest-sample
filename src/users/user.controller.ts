import { Controller, Post, Body, Req, Res, HttpStatus, Get, UseGuards, HttpException, UseInterceptors, UploadedFile, Param } from '@nestjs/common';
import { Request, Response } from 'express';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { AuthRequest } from 'src/interfaces/request.interface';
import { AuthService } from 'src/auth/auth.service';
import { UserService } from 'src/users/user.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { createDirectory, imageFileFilter, PROFILE_DIRECTORY, uploadOptions } from 'src/utils/upload.utils';
import { join } from 'path';
import { isEmpty } from 'lodash';
import { existsSync } from 'fs';
import { UserDTO } from './dtos/user.dto';

@Controller('users')
export class UserController {
  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) {}

  @Post('login')
  async login(@Req() req: Request, @Res({ passthrough: true }) res: Response, @Body() body : UserDTO): Promise<Response> {
    try {
      const user = await this.userService.findUser(
        body.username,
        body.password,
      );
      if (!user) {
        return res.status(HttpStatus.BAD_REQUEST).json({
          message: 'Invalid username or password',
          status: HttpStatus.BAD_REQUEST,
        });
      }
      const accessToken = await this.authService.createToken(user);

      if (!accessToken) throw new HttpException('Error generating access token', HttpStatus.INTERNAL_SERVER_ERROR);

      res.cookie('token', accessToken);
      
      return res.status(HttpStatus.OK).json({
        message: 'Logged in',
        status: HttpStatus.OK,
        data: {
          accessToken,
        },
      });
    } catch (error) {
      console.log('Error', JSON.stringify(error));
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error logging in',
        status: HttpStatus.BAD_REQUEST,
      });
    }
  }

  @Post('register')
  async register(@Req() req: Request, @Res() res: Response, @Body() body : UserDTO): Promise<Response> {
    try {
      const user = await this.userService.create(
        body['username'],
        body['password'],
      );
      if (!user) {
        return res.status(HttpStatus.BAD_REQUEST).json({
          message: 'Reistration unsuccessful',
          status: HttpStatus.BAD_REQUEST,
        });
      }
      return res.status(HttpStatus.OK).json({
        message: 'Registration successful',
        status: HttpStatus.OK,
      });
    } catch (error) {
      console.log('Error', JSON.stringify(error, null, '\t'));
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error registration',
        status: HttpStatus.BAD_REQUEST,
      });
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('logout')
  async logout(@Req() req: AuthRequest, @Res() res: Response): Promise<Response> {
    try {
      const update = await this.userService.update(req.user.sub, { currenttoken: null });

      if (!update) throw new HttpException('Error logging out', HttpStatus.INTERNAL_SERVER_ERROR);

      res.clearCookie('token');
      
      return res.status(HttpStatus.OK).json({
        message: 'Logged out',
        status: HttpStatus.OK,
      });
    } catch (error) {
      console.log('Error', JSON.stringify(error));
      return res.status(HttpStatus.BAD_REQUEST).json({
        message: 'Error logging out',
        status: HttpStatus.BAD_REQUEST,
      });
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  async profile(@Req() req: AuthRequest, @Res() res: Response) {
    const user = await this.userService.findUserByData({ userid: req.user.sub });

    if (!user) throw new HttpException('Error fetching profile', HttpStatus.BAD_REQUEST);

    return res.status(HttpStatus.OK).json({
      message: 'Success fetching profile',
      data: {
        user,
      }
    });
  }

  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('profile', uploadOptions(createDirectory(PROFILE_DIRECTORY), imageFileFilter)))
  @Post('profile/upload')
  async upload(@Req() req: AuthRequest, @Res() res: Response, @UploadedFile() pic: Express.Multer.File) {
    if (!pic) throw new HttpException('No file attached', HttpStatus.BAD_REQUEST);
    // sample
    // {
    //   fieldname: 'profile',
    //   originalname: 'Screenshot_20220330_081736.jpg',
    //   encoding: '7bit',
    //   mimetype: 'image/jpeg',
    //   destination: './uploads/profile',
    //   filename: 'Screenshot_20220330_081736-fbb1.jpg',
    //   path: 'uploads/profile/Screenshot_20220330_081736-fbb1.jpg',
    //   size: 250481
    // }

    const update = await this.userService.update(req.user.sub, {
      profilepic: pic.filename,
    });

    if (!update) throw new HttpException('No file attached', HttpStatus.BAD_REQUEST);
    
    return res.status(HttpStatus.OK).json({
      message: 'Success uploading file',
      data: {
        originalname: pic.originalname,
        filename: pic.filename,
        url: join(req.get('Host'), pic.path)
      }
    });
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile/upload')
  async uploadedFile(@Req() req: AuthRequest, @Res() res: Response) {
    const user = await this.userService.findUserByData({ userid: req.user.sub });

    if (!user || (user && isEmpty(user.profilepic))) throw new HttpException('Error fetching profile pic', HttpStatus.BAD_REQUEST);
    if (!existsSync(join(PROFILE_DIRECTORY, user.profilepic))) throw new HttpException('File not found', HttpStatus.NOT_FOUND);

    return res.sendFile(user.profilepic, { root: PROFILE_DIRECTORY });
  }
}
