import { DATE } from 'sequelize';
import { Column, HasMany, Model, Table } from 'sequelize-typescript';
import { Product } from 'src/products/products.model';

@Table({ tableName: 'test_users' })
export class User extends Model {
  @Column({ primaryKey: true, autoIncrement: true, allowNull: false })
  userid: number;

  @Column({ allowNull: false, unique: true })
  username: string;

  @Column({ allowNull: false })
  password: string;

  @Column({ allowNull: true, defaultValue: null })
  currenttoken: string;

  @Column({ defaultValue: new Date(), allowNull: false })
  datecreated: boolean;

  @Column({ allowNull: true })
  profilepic: string;

  @HasMany(() => Product, 'userid')
  products: Product[]  
}
