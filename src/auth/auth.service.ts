import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { UserService } from '../users/user.service';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/users/user.model';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UserService,
    private jwtService: JwtService,
  ) {}

  async createToken(user: any) {
    const payload = { username: user.username, sub: user.userid };
    const token = this.jwtService.sign(payload);
    // console.log('Access token', token);
    const update = await this.usersService.update(user.userid, { currenttoken: token })
    if (!update) return null;
    return token;
  }

  async validateUser(payload: { username: string, sub: number }): Promise<User> {

    if (!payload.username || !payload.sub) throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);  
    const user = await this.usersService.findUserByData({ username: payload.username, userid: payload.sub });

    if (!user) throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);    
    return user;  
  }
}
