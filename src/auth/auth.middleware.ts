import { HttpException, HttpStatus, Injectable, NestMiddleware } from "@nestjs/common";
import { Request, Response, NextFunction } from "express";
import { UserService } from "src/users/user.service";

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(private readonly usersService: UserService) {}
  async use(req: Request, res: Response, next: NextFunction) {
    if (!req.headers.authorization) {
      res.clearCookie('token');
      throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
    }

    // get jwt token
    const token = req.headers.authorization?.split(' ')[1];
    
    const sessionuser = await this.usersService.findUserByToken(token);
    // console.log('Session user from middleware', sessionuser)

    if (!sessionuser) {
      res.clearCookie('token');
      throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED);
    }
    next();
  }
}