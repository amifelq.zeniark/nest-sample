import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { AuthService } from 'src/auth/auth.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET_KEY,
    });
  }

  async validate(payload: { username: string, sub: number }) {
    // console.log('payload', payload);
    const user = await this.authService.validateUser(payload);
    
    if (!user) {
      throw new HttpException('User not found', HttpStatus.NOT_FOUND);    
    }
    return { sub: payload.sub, username: payload.username };
  }
}
