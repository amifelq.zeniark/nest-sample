import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Sequelize } from 'sequelize-typescript';
import { User } from 'src/users/user.model';
import { Product } from './products.model';

@Injectable()
export class ProductsService {
  constructor(@InjectModel(Product) private productModel: typeof Product, @InjectModel(User) private userModel: typeof User) {}
  
  create(name: string, type: number, userid: number ) : Promise<Product> {
    // console.log(name, type, userid)
    return this.productModel.create({
      prodname: name,
      prodtype: type,
      userid,
    });
  }

  async findAll(offset: number, limit: number) {
    let options : any = {};
    if (limit && offset) options.offset = parseInt(`${offset}`, 10);
    if (limit) options.limit = parseInt(`${limit}`, 10);
    // console.log('Options', options)
    let products = await this.productModel.findAll({
      ...options,
      include: [{
        model: User,
        where: {
          userid: Sequelize.col('user.userid')
        },
        attributes: {
          exclude: ['password', 'currenttoken', 'profilepic']
        },
      }],
    });
    return products;
  }

  findOne(id: number) {
    return this.productModel.findOne({
      where: {
        prodid: id,
      },
      include: [{
        model: User,
        where: {
          userid: Sequelize.col('user.userid')
        },
        attributes: {
          exclude: ['password', 'currenttoken']
        },
      }],
    });
  }

  async update(id: number, data: any): Promise<boolean> {
    const query = await this.productModel.update(data, {
      where: {
        prodid: id, 
      }
    });
    // console.log(JSON.stringify(query[0], null, '\t'));
    if (query[0] > 0) return true;
    return false
  }

  async remove(id: number) {
    const deletedRows = await this.productModel.destroy({
      where: {
        prodid: id,
      }
    });
    if (deletedRows > 0) return true;
    return false;
  }
}
