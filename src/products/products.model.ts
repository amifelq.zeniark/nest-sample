import { BelongsTo, Column, ForeignKey, HasOne, Length, Model, Table } from 'sequelize-typescript';
import { User } from 'src/users/user.model';

@Table({ tableName: 'test_products' })
export class Product extends Model {
  @Column({  primaryKey: true, autoIncrement: true, allowNull: false })
  prodid: number;

  @Column({ allowNull: false })
  prodname: string;
  
  @Length({ min: 1, max: 2 })
  @Column({ allowNull: false })
  prodtype: number;

  @Column({ allowNull: false })
  @ForeignKey(() => User)
  userid: number;

  @BelongsTo(() => User, 'userid')
  user: User;
}