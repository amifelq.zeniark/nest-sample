import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsAlphanumeric, IsInt, IsNotEmpty, IsNumber, Max, Min } from 'class-validator';

export class ProductDTO {
  @ApiProperty()
  @IsNotEmpty({ message: 'Product name is required' })
  @IsAlphanumeric('en-US',{ message: 'Product name must include letters and numbers only'})
  name: string;

  @ApiProperty()
  @IsNotEmpty({ message: 'Type is required' })
  @IsNumber({}, { message: 'Type must be a number' })
  @Min(1, { message: 'Type must be greater than or equal to 1' })
  @Max(2, { message: 'Type must be lesser than or equal to 2' })
  @Type(() => Number)
  type: number;
}
