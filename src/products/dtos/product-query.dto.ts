import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsAlphanumeric, IsInt, IsNotEmpty, IsNumber, IsOptional, Max, Min } from 'class-validator';

export class ProductQueryDTO {
  @ApiProperty()
  @IsOptional()
  @IsNotEmpty()
  @IsInt({ message: 'Limit must be a number' })
  @Type(() => Number)
  limit: number;

  @ApiProperty()
  @IsOptional()
  @IsNotEmpty()
  @IsInt({ message: 'Offset must be a number' })
  @Type(() => Number)
  offset: number;
}
