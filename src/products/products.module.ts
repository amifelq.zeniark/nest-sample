import { Module } from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductsController } from './products.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Product } from './products.model';
import { User } from 'src/users/user.model';

@Module({
  imports: [
    SequelizeModule.forFeature([Product, User])
  ],
  controllers: [ProductsController],
  providers: [ProductsService]
})
export class ProductsModule {}
