'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
     await queryInterface.createTable('test_products', {
      prodid: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      prodname: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      prodtype: {
        type: Sequelize.INTEGER,
        defaultValue: 1,
        comment: '(1 - Default, 2 - Custom)',
      },
      userid: {
        type: Sequelize.INTEGER,
        references: {
          model: 'test_users',
          key: 'userid',
        }
      }
    });
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.dropTable('test_products');
  }
};
