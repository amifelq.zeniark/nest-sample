# nest-sample

Nest JS + TS with SOLID principles (hopefully)

## Installation

Install dependencies thru npm

```bash
npm i
```

## Database setup

1. Create test MySQL database

2. Use database and then manually add `test_users` table

```bash
CREATE TABLE test_users (
userid INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
username VARCHAR(25) NOT NULL UNIQUE, 
password VARCHAR(255) NOT NULL, 
datecreated DATETIME NOT NULL DEFAULT NOW(),
currenttoken VARCHAR(255) NULL
);
```

3. Change database credentials in `src/app.module.ts` and in `config/config.json` (for sequelize)

4. Run migrations, ensure `config/config.json` is already pointed to test database
```bash
npx sequelize-cli db:migrate
```

## Running the app

Start app (assuming database server is running)
- On dev ENV:
```bash
npm run start:dev
```

## Todo

- [ ]   Create .env variables for database credentials
- [ ]   Fix migrations to add `test_users` table
- [ ]   Create DTOs for validation on `Users` and `Products`, connect validation pipe